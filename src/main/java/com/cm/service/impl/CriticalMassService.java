package com.cm.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import com.cm.service.ICriticalMassService;

@Service
@RefreshScope
public class CriticalMassService implements ICriticalMassService {

	@Value("${cm.message.test}")
	private String messageTest;

	public String getMessage() throws Exception {
		return messageTest;
	}

}
