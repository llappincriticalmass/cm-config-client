package com.cm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cm.service.ICriticalMassService;

import org.springframework.cloud.context.config.annotation.RefreshScope;

@RestController
public class CriticalMassController {

	@Autowired
	private ICriticalMassService criticalMassService;

	@GetMapping("/message")
	@ResponseBody
	public String getMessage() throws Exception {
		return criticalMassService.getMessage();
	}

}
